var page_size = 0;
var memory_size = 0;
var page_count = 0;
var alphabet = ("abcdefghijklmnopqrstuvwxyz").split("");
var frame_list = [];

var logical_data = {};

jQuery.fn.clearContent = function() {
  $(this).html('');
  return $(this);
}

// Izveidot tabulas.
function process_tables(){
  set_frame_list();
  if(page_size <= memory_size){
    $("#logical-table").clearContent().append(build_logical_table());
    $("#physical-table").clearContent().append(build_physical_table());
    $("#page-table").clearContent().append(build_page_table());
    $(".table-container").show();
  }
}

// Paņemt nejaušu burtu no alfabēta.
function rand_letter(){
  return alphabet[Math.floor(Math.random()*alphabet.length)];
}

// Masīva jaukšanas funkcija.
function shuffle_array(theArray) {
  var len = theArray.length;
  var i = len;
  while (i--) {
    var p = parseInt(Math.random()*len);
    var t = theArray[i];
    theArray[i] = theArray[p];
    theArray[p] = t;
  }
};

// Izveidot patvaļīgu rāmju sarakstu.
function set_frame_list(){
  frame_list = [];
  for(i=1; i <= page_count; i++){
    frame_list.push(i);
  }
  shuffle_array(frame_list);
}

//Izveidot loģiskās atmiņas tabulu.
function build_logical_table(){
  table = "";
  page_size_counter = 0;
  page_content = [];
  logical_data = {};
  page_offset = 0;
  for(i=0; i < memory_size; i++){
    if(i % page_size == 0){
      table += "<tr>";
      table += "<td class='logical_counter' rowspan='" + page_size + "'>" + page_size_counter + "</td>";
      page_size_counter++;
      page_offset = 0;
    }else
      table += "<tr>";

    table += "<td class='memory_data' offset='" + page_offset +  "' page='" + String(page_size_counter - 1) +"'>" + i + "</td>";
    letter = rand_letter();
    page_content.push(letter);
    table += "<td class='memory_data' offset='" + page_offset +  "' page='" + String(page_size_counter - 1) +"'>" + letter + "</td>";
    table += "</tr>";
    page_offset++;
    // Izveidot loģiskās atmiņas saturu objektā.
    if( logical_data[page_size_counter] == null )
      logical_data[page_size_counter] = [letter];
    else
      logical_data[page_size_counter].push(letter);
  }
  return table;
}

// Izveidot lapu tabulu.
function build_page_table(){
  table = "";
  $.each(logical_data, function(j, b){
    table += "<tr>";
    table += "<td>" + String(j-1) + "</td>";
    table += "<td>" + String(b-1) + "</td>";
    table += "</tr>";
  });
  return table;
}

//Izveidot fiziskās atmiņas tabulu.
function build_physical_table(){
  shuffle_array(frame_list);
  table = "";
  frame_size_counter = 0;
  byte_counter = 0;
  $.each(frame_list, function(i, a){
    rowspan = true;
    $.each(logical_data[a], function(j, b){
      if(rowspan == true){
        table += "<tr>";
        table += "<td class='phys_counter' " + "rowspan='"+ page_size + "'>" + frame_size_counter + "</td>";
        table += "<td>" + byte_counter + "</td>";
        table += "<td frame='" +  i  +"'>" + b + "</td>";
        table += "</tr>";
        frame_size_counter += page_size;
        rowspan = false;
      }else{
        table += "<tr>";
        table += "<td>" + byte_counter + "</td>";
        table += "<td frame='" +  i     +"'>" + b + "</td>";
        table += "</tr>";
      }
      byte_counter++;
    })
    logical_data[a] = i+1;
  })
  return table;
}

// Informācijas logs
function show_info_table(page, page_address, frame, offset){
  $('#info-page').html(page);
  $('#info-log-address').html(page_address);
  $('#info-frame').html(frame);
  $('#info-offset').html(offset);

  phys_address = String((parseInt(frame) * parseInt(page_size)) + parseInt(offset));
  $('#info-formula').html(
    "(" + frame + " * " + page_size + ")" + " + " + offset + " = " + phys_address
  );

  $(".info-table").show();
}

/***********************************************************/

$(function() {
  var sizes = ["0","0","2","4","8","16","32","64","128","256","512"];
  $( "#slider-range-page" ).slider({
    range: "min",
    value: 2,
    min: 2,
    max: sizes.length - 1,
    slide: function( event, ui ) {
      $( "#page_size" ).val(sizes[ui.value] );
    }
  });
  $( "#page_size" ).val( $( "#slider-range-page" ).slider( "value" ) );

  $( "#slider-range-memory" ).slider({
    range: "min",
    value: 2,
    min: 2,
    max: sizes.length - 1,
    slide: function( event, ui ) {
      $( "#memory_size" ).val(sizes[ui.value]  );
    }
  });
  $( "#memory_size" ).val( $( "#slider-range-memory" ).slider( "value" ) );

  $("#set-form").submit(function(){
    page_size = parseInt($( "#page_size" ).val());
    memory_size = parseInt($( "#memory_size" ).val());
    page_count = memory_size / page_size;
    process_tables();
    return false;
  });

  $(".memory_data").live('mouseover', function() {
    if(parseInt($(this).siblings('td').html()) >= 0)
      page_address = $(this).siblings('td').html();
    else
      page_address = $(this).html();

    $(this).parent().find('td').css("background-color", "#FFDEB8");
    page = $(this).attr('page');
    offset = $(this).attr('offset');
    frame = $("#page-table").children().eq(page).find('td')
    frame.css("background-color", "#FFDEB8");
    frame_number = frame[1].innerHTML;
    $("#physical-table").children().find("[frame='" + frame_number + "']").eq(offset).parent().find('td').css("background-color", "#FFDEB8");

    show_info_table(page, page_address, frame_number, offset);
  });

  $(".memory_data").live('mouseout', function() {
    $(this).parent().find('td').css("background-color", "");
    page = $(this).attr('page');
    offset = $(this).attr('offset');
    frame = $("#page-table").children().eq(page).find('td')
    frame.css("background-color", "");
    frame_number = frame[1].innerHTML;
    $("#physical-table").children().find("[frame='" + frame_number + "']").eq(offset).parent().find('td').css("background-color", "");
    $(".info-table").hide();
  });

});
