// Izveido galveno zīmēšanas plakni.
var paper = new Raphael(document.getElementById('canvas_container'), 900, 200);  

var stack = 0;
var process_address = 0;
var base_address = 0;

// Izveido CPU grupu, kas satur taisnstūri un tekstu.
var cpu = paper.set();
  cpu_elem = paper.rect(0, 0, 100, 50);
  cpu_text = paper.text(50, 25, "CPU").attr({fill: '#000000', 'font-size': '14px'})
  cpu.push(cpu_elem);
  cpu.push(cpu_text);
  cpu.translate(100,50);

// Izveido MMU grupu, kas satur taisnstūri un tekstu.
var mmu = paper.set();
  mmu_elem = paper.rect(0, 0, 100, 200);
  mmu_text = paper.text(50, 25, "MMU").attr({fill: '#000000', 'font-size': '14px'})
  mmu.push(mmu_elem);
  mmu.push(mmu_text);
  mmu.translate(300,0);

// Izveido atmiņas grupu, kas satur taisnstūri un tekstu.
var memory = paper.set();
  memory_elem = paper.rect(0, 0, 100, 200);
  memory_text = paper.text(50, 25, "Atmiņa").attr({fill: '#000000', 'font-size': '14px'})
  memory.push(memory_elem);
  memory.push(memory_text);
  memory.translate(500,0);



function initiate_process(){

  var process = paper.set();
    process_elem = paper.rect(100, 65, 80, 20).attr({fill: '#ffffff'});
    process_text = paper.text(140, 77, process_address).attr({fill: '#000000', 'font-size': '14px', 'text-anchor': 'start'});



    process_elem.animate({x: 310}, 3000, function() {
        // phys_address = parseInt(process_address) + parseInt(base_address);
        // process_elem.attr({text: phys_address });
      this.animate({x: 510}, 3000);
    });
    
    process_text.animate({x: 350}, 3000, function() {
        phys_address = parseInt(process_address) + parseInt(base_address);
        process_text.attr({text: phys_address });
      this.animate({x: 550}, 3000);
    });
    

}


(function(){
  $("#set-da-params").submit(function(){
    base_address = $(this).find("input[name='base_register']").val() || 0;
    process_address = $(this).find("input[name='process_register']").val() || 0;
    initiate_process();
    return false;
  });
})();